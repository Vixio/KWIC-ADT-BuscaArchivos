import Kommander.MasterKommander;

public class Main {
    public static void main (String args[]) {
        MasterKommander masterKommander = new MasterKommander();

        if ( args.length < 2 ) {
            System.out.println("Usage: java Main <keywordsFile> <directoryToSearch>");
            return;
        }
        masterKommander.process(args[0], args[1]);
    }
}
