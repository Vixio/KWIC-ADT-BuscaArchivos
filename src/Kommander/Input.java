package Kommander;

import Kommander.impl.CharactersImpl;
import Kommander.impl.FilesImpl;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Input {
    public void run(String keywordFileName, String directory) {
        try {
            Scanner scanner = new Scanner(new File(keywordFileName));
            while(scanner.hasNext()) {
                String line = scanner.nextLine().trim();

                if ( ! line.isEmpty() ) {
                    CharactersImpl.getInstance().addWord(line);
                }
            }

            addDirectory(directory);

        } catch (IOException e) {
        }
    }

    public void addDirectory(String directory) {
        File folder = new File(directory);

        if ( ! folder.isDirectory() ) {
            System.err.println("El directorio especificado no es una ruta valida");
            return;
        }

        FilesImpl filesADT = FilesImpl.getInstance();
        File[] files = folder.listFiles();
        for ( int i = 0; i < files.length; i++ ) {
            if ( files[i].isDirectory() ) {
                addDirectory(files[i].getAbsolutePath());
            }
            else if( ! files[i].getName().startsWith(".") ) {
                filesADT.addFileName( files[i].getAbsolutePath() );
            }
        }
    }
}
