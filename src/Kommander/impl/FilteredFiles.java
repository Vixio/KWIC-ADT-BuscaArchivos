package Kommander.impl;

import java.util.ArrayList;
import java.util.HashSet;

public class FilteredFiles {
    private static FilteredFiles sInstance;

    private ArrayList<String> filteredFiles;

    private FilteredFiles() {
        filteredFiles = new ArrayList<>();
    }

    public void setup() {
        FilesImpl filesADT = FilesImpl.getInstance();


        for ( int i =0 ; i < filesADT.len(); i++ ) {
            String filepath = filesADT.getFileName(i);

            int lastDash = filepath.lastIndexOf('/');
            String filename = filepath.substring(lastDash + 1, filepath.length());

            CharactersImpl keywordsADT = CharactersImpl.getInstance();
            for ( int j = 0; j < keywordsADT.len(); j++ ) {
                String keyword = keywordsADT.getWord(j);

                String[] words = filename.split(" ");
                for ( int k = 0; k < words.length; k++ ) {
                    String word = words[k];

                    int lastDotIndex = word.lastIndexOf(".");
                    if ( lastDotIndex != -1 ) {
                        word = word.substring(0, lastDotIndex);
                    }

                    if ( keyword.equals( word ) ) {
                        filteredFiles.add(filepath);
                    }
                }
            }
        }

    }

    public String iTh(int index) {
        return filteredFiles.get(index);
    }

    public int len() {
        return filteredFiles.size();
    }

    public static FilteredFiles getInstance() {
        if ( sInstance == null ) {
            sInstance = new FilteredFiles();
        }

        return sInstance;
    }
}
