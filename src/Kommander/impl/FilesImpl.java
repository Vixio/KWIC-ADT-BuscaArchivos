package Kommander.impl;

import java.util.ArrayList;

public class FilesImpl {

    private ArrayList<String> filenames;
    private static FilesImpl sInstance;

    private FilesImpl() {
        filenames = new ArrayList<>();
    }

    public void addFileName(String file) {
        filenames.add(file);
    }

    public String getFileName(int index) {
        return filenames.get(index);
    }

    public int len() {
        return filenames.size();
    }

    public static FilesImpl getInstance() {
        if ( sInstance == null ) {
            sInstance = new FilesImpl();
        }

        return sInstance;
    }
}
