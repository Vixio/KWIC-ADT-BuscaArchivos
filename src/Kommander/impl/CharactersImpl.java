package Kommander.impl;

import Kommander.adt.Characters;

import java.util.ArrayList;;

public class CharactersImpl implements Characters {

    private ArrayList<String> words;
    private static CharactersImpl sInstance;

    private CharactersImpl() {
        words = new ArrayList<>();
    }

    @Override
    public void addWord(String word) {
        words.add(word);
    }

    @Override
    public String getWord(int i) {
        return words.get(i);
    }

    @Override
    public int len() {
        return words.size();
    }

    public static CharactersImpl getInstance() {
        if ( sInstance == null ) {
            sInstance = new CharactersImpl();
        }

        return sInstance;
    }

}
