package Kommander;

import Kommander.impl.FilteredFiles;

public class MasterKommander {
    private Input input;
    private Output output;


    public MasterKommander() {
        input = new Input();
        output = new Output();
    }

    public void process(String keywordFileName, String directory){
        input.run(keywordFileName, directory);
        FilteredFiles.getInstance().setup();
        output.run();
    }
}
