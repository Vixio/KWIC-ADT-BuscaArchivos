package Kommander.adt;

public interface Characters {

    void addWord(String word);

    String getWord(int i);

    int len();
}
